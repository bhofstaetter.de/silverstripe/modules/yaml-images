<?php

namespace bhofstaetter\YamlImages;

use SilverStripe\Admin\LeftAndMain;
use SilverStripe\Control\Controller;
use SilverStripe\Dev\DevBuildController;
use SilverStripe\Dev\DevelopmentAdmin;
use SilverStripe\ORM\DatabaseAdmin;

class Toolbox
{
    public static function is_backend(): bool
    {
        if (!Controller::has_curr()) {
            return false;
        }

        $ctrl = Controller::curr();

        if (
            $ctrl instanceof LeftAndMain
            || is_subclass_of($ctrl, LeftAndMain::class)
            || self::on_dev_build()
        ) {
            return true;
        }

        return false;
    }

    public static function on_dev_build(): bool
    {
        if (!Controller::has_curr()) {
            return false;
        }

        $ctrl = Controller::curr();

        if (
            $ctrl instanceof DevelopmentAdmin
            || $ctrl instanceof DatabaseAdmin
            || $ctrl instanceof DevBuildController
            || is_subclass_of($ctrl, DevelopmentAdmin::class)
            || is_subclass_of($ctrl, DatabaseAdmin::class)
            || is_subclass_of($ctrl, DevBuildController::class)
        ) {
            return true;
        }

        return false;
    }
}
