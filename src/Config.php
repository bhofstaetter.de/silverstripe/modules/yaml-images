<?php

namespace bhofstaetter\YamlImages;

use Psr\SimpleCache\CacheInterface;
use SilverStripe\Control\Director;
use SilverStripe\Core\Config\Configurable;
use SilverStripe\Core\Flushable;
use SilverStripe\Core\Injector\Injector;
use SilverStripe\Versioned\Versioned;

class Config implements Flushable
{
    use Configurable;

    protected static string $cacheClassName = CacheInterface::class . '.YamlImagesConfigs';
    protected string $cacheKey;
    protected CacheInterface $cache;
    protected string $configName;

    public function __construct(string $configName, string $cacheKey)
    {
        $this->cache = Injector::inst()->get(self::$cacheClassName);
        $this->cacheKey = $cacheKey;
        $this->configName = $configName;
    }

    public static function inst(
        string $configName,
        bool   $lazyRetina = null,
        int    $imgID = null
    ): self {
        $parts = [
            $configName,
            $lazyRetina,
            $imgID,
            Director::host(),
            Versioned::get_reading_mode()
        ];

        return new Config($configName, str_replace('\\', '_', implode('_', $parts)));
    }

    public static function flush(): void
    {
        Injector::inst()->get(self::$cacheClassName)->clear();
    }

    public function getCachedData(): mixed // todo
    {
        return $this->cache->has($this->cacheKey)
            ? $this->cache->get($this->cacheKey)
            : null;
    }

    public function setCachedData($data): self // todo
    {
        $this->cache->set($this->cacheKey, $data);
        return $this;
    }

    public function deleteCachedData(string $cacheKey = null): self // todo
    {
        $this->cache->delete($cacheKey ?: $this->cacheKey);
        return $this;
    }

    public function getYamlConfiguration() {
        return self::config()->get($this->configName);
    }
}
