<?php

namespace bhofstaetter\YamlImages;

use SilverStripe\Assets\Image;
use SilverStripe\Control\Controller;
use SilverStripe\Core\Path;

class ApiV1Controller extends Controller
{
    private static $url_segment = 'api/v1/yaml-images';

    private static $allowed_actions = [
        'lazyImage'
    ];

    public function lazyImage() {
        $imageID = $this->getRequest()->param('ImageID');
        $configName = base64_decode(urldecode($this->getRequest()->param('ConfigName')));
        $retina = (bool) $this->getRequest()->getVar('2x');

        if (str_starts_with($imageID, 'base64_')) {
            $img = Base64Image::create();
        } else {
            $img = Image::get()->byID($imageID);
        }

        if (!$img->exists()) {
            return $this->httpError(404);
        }

        $yamlImg = $img->YamlConfig($configName, $retina, false);

        if ($yamlImg instanceof Base64Image) {
            $contents = base64_decode($yamlImg->getBase64EncodedContent());
            $fileSize = strlen($contents);
        } else {
            $imagePath = Path::join(ASSETS_PATH, str_replace(ASSETS_DIR, '', $yamlImg->YamlImage->Link()));
            $fileSize = filesize($imagePath);
            $contents = file_get_contents($imagePath);
        }

        $this->getResponse()
            ->addHeader('Content-Type', $img->getMimeType())
            ->addHeader('Content-Length', $fileSize)
            ->setBody($contents)
            ->output();
    }

    public static function link_to($action = null): string {
        return ApiV1Controller::singleton()->Link($action);
    }
}
