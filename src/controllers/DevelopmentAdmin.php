<?php

namespace bhofstaetter\YamlImages;

use SilverStripe\Control\Director;
use SilverStripe\Dev\DebugView;
use SilverStripe\Dev\DevBuildController;

class DevelopmentAdmin extends DevBuildController
{
    private static $url_handlers = [
        '' => 'build'
    ];

    private static $allowed_actions = [
        'build'
    ];

    public function build($request)
    {
        parent::build($request);

        $this->buildYamlImagesConfigFile();

        if (!Director::is_cli()) {
            $renderer = DebugView::create();
            echo $renderer->renderParagraph('Yaml Images Auto Config created');
        }
    }

    public function buildYamlImagesConfigFile()
    {
        $path = null;
        $theme = null;

        $this->extend('updateBuildYamlImagesConfigFile', $path, $theme);

        $autoConfig = new AutoConfig();
        $autoConfig->build($path, $theme);
    }
}
