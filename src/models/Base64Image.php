<?php

namespace bhofstaetter\YamlImages;

use Psr\SimpleCache\CacheInterface;
use SilverStripe\Assets\Image;
use SilverStripe\Core\Injector\Injector;
use SilverStripe\ORM\HiddenClass;
use SilverStripe\View\TemplateGlobalProvider;

class Base64Image extends Image implements HiddenClass, TemplateGlobalProvider
{
    protected int $width;
    protected int $height;

    protected static string $cacheClassName = CacheInterface::class . '.YamlImagesBase64Images';

    public function __construct($width = 1, $height = 1)
    {
        $this
            ->setWidth($width)
            ->setHeight($height);

        parent::__construct(null, true, []);
    }

    public function setWidth($val): self
    {
        $this->width = (int) $val;
        return $this;
    }

    public function setHeight($val): self
    {
        $this->height = (int) $val;
        return $this;
    }

    public function getWidth(): int
    {
        return $this->width ?: $this->height;
    }

    public function getHeight(): int
    {
        return $this->height ?: $this->width;
    }

    public function base64String(): string|null
    {
        $width = $this->getWidth();
        $height = $this->getHeight();

        if (!$width || !$height) {
            return null;
        }

        $cache = Injector::inst()->get(self::$cacheClassName);
        $cacheKey = $width . '_' . $height;

        if ($cache->has($cacheKey)) {
            return $cache->get($cacheKey);
        }

        ob_start();

        $img = imagecreatetruecolor($width, $height);
        imagetruecolortopalette($img, false, 1);
        imagepng($img, null, 9);
        $imgData = ob_get_contents();

        ob_end_clean();

        $string = base64_encode($imgData);
        $cache->set($cacheKey, $string);

        return $string;
    }

    public function getURL($grant = true): string
    {
        return 'data:image/png;base64,' . $this->base64String();
    }

    public function URL(): string
    {
        return $this->getURL();
    }

    public function getAbsoluteURL(): string
    {
        return $this->getURL();
    }

    public function PreviewLink($action = null): string
    {
        return Base64Image::create(
            $this->config()->get('asset_preview_width'),
            $this->config()->get('asset_preview_height')
        )->getURL();
    }

    public function Base64ImageID(): string
    {
        return uniqid('base64_');
    }

    public function ID(): string
    {
        return $this->Base64ImageID();
    }

    public function exists(): bool
    {
        return true;
    }

    public function getMimeType(): string
    {
        return 'image/png';
    }

    public static function get_template_global_variables(): array
    {
        return [
            'Base64Image' => 'create',
        ];
    }
}

