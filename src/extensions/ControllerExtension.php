<?php

namespace bhofstaetter\YamlImages;

use SilverStripe\Core\Extension;

class ControllerExtension extends Extension
{
    public function getYamlImagesPrefix()
    {
        $prefix = null;
        $exclude = Config::config()->get('exclude_from_prefix');

        $this->owner->extend('updateYamlImagesPrefix', $prefix, $exclude);

        return [
            'prefix' => $prefix,
            'exclude' => $exclude,
        ];
    }
}
