<?php

namespace bhofstaetter\YamlImages;

use SilverStripe\Assets\Image;
use SilverStripe\Control\Controller;
use SilverStripe\Core\Path;
use SilverStripe\ORM\ArrayLib;
use SilverStripe\ORM\ArrayList;
use SilverStripe\ORM\DataExtension;
use SilverStripe\ORM\FieldType\DBHTMLText;

class ImageExtension extends DataExtension
{
    public function getYamlConfigName(string $desiredConfigName): string
    {
        $ctrl = Controller::curr();

        if (!$ctrl || !$ctrl->hasMethod('getYamlImagesPrefix')) {
            return $desiredConfigName;
        }

        $data = $ctrl->getYamlImagesPrefix();

        if (!isset($data['prefix'])) {
            return $desiredConfigName;
        }

        if (!isset($data['exclude'])) {
            return $data['prefix'] . $desiredConfigName;
        }

        if (in_array($desiredConfigName, $data['exclude'])) {
            return $desiredConfigName;
        }

        return $data['prefix'] . $desiredConfigName;
    }

    public function YamlConfig(
        string $desiredConfig,
        bool   $retina = false,
        bool   $render = true
    ): mixed {
        $configName = $this->getYamlConfigName($desiredConfig);

        $config = Config::inst(
            $configName,
            $retina,
            $this->owner->ID
        );

        $data = $config->getCachedData()
            ? $config->getCachedData()
            : $this->createData($config, $configName);

        return $this->generateImage($data, $render, $retina);
    }

    private function createData(Config $config, string $configName): array
    {
        $yamlConfiguration = $config->getYamlConfiguration();
        $data = [
            'configName' => $configName,
        ];

        if (isset($yamlConfiguration['breakpoints']) && $yamlConfiguration['breakpoints']) {
            $data['breakpoints'] = $this->createBreakpoints($yamlConfiguration['breakpoints']);
        } else {
            $data['breakpoints'] = false;
        }

        unset($yamlConfiguration['breakpoints']);


        $manipulations = self::get_manipulations($yamlConfiguration);
        $data['manipulations'] = count($manipulations) ? $manipulations : false;

        $config->setCachedData($data);

        return $data;
    }

    public static function get_manipulations(array $yamlConfiguration): array
    {
        if (isset($yamlConfiguration['order']) && $yamlConfiguration['order']) {
            $data['order'] = $yamlConfiguration['order'];
            unset($yamlConfiguration['order']);

            $order = explode(',', str_replace(' ', '', $data['order']));
            $manipulations = array_merge(array_flip($order), $yamlConfiguration);
            $manipulations = array_intersect_key($manipulations, array_flip($order));
        } else {
            unset($yamlConfiguration['order']);
            $manipulations = $yamlConfiguration;
        }

        return $manipulations;
    }

    private function createBreakpoints(array $queryAndConfig): ArrayList
    {
        $breakpoints = ArrayList::create();

        foreach ($queryAndConfig as $query => $configName) {
            $breakpoints->add([
                'MediaQuery' => $query,
                'LazyLink' => $this->createLazyLink($configName),
                'Dimensions' => $this->getDimensions($configName),
            ]);
        }

        return $breakpoints;
    }

    private function createLazyLink(string $configName): string
    {
        $url = ApiV1Controller::link_to('lazyImage');

        if ($this->owner->hasMethod('Base64ImageID')) {
            $id = $this->owner->Base64ImageID();
        } else {
            $id = $this->owner->ID;
        }

        return Controller::join_links($url, $id, urlencode(base64_encode($configName)));
    }

    private function generateImage(array $data, bool $render, bool $retina)
    {
        $img = $this->owner;

        if (!$img->exists() || !$data['manipulations']) {
            return $img;
        }

        $isBase64Image = $img instanceof Base64Image;
        $multiplier = ($retina && !$isBase64Image) ? 2 : 1;

        if ($isBase64Image) {
            $lastManipulation = explode(',', end($data['manipulations']));
            $width = $lastManipulation[0];
            $height = $lastManipulation[1] ?? $width;

            $img = $img
                ->setWidth($width)
                ->setHeight($height);
        } else {
            foreach ($data['manipulations'] as $manipulation => $arguments) {
                $arguments = explode(',', str_replace(' ', '', $arguments));
                $count = count($arguments);

                if ($retina && $this->owner->hasMethod($manipulation . 'Max')) {
                    $manipulation = $manipulation . 'Max';
                }

                if ($count === 1) {
                    $img = $img->$manipulation((int)$arguments[0] * $multiplier);
                } else if ($count === 2) {
                    $img = $img->$manipulation(
                        (int)$arguments[0] * $multiplier,
                        (int)$arguments[1] * $multiplier
                    );
                } else if ($count === 3) {
                    $img = $img->$manipulation(
                        (int)$arguments[0] * $multiplier,
                        (int)$arguments[1] * $multiplier,
                        $arguments[2]
                    );
                } else if ($count === 4) {
                    $img = $img->$manipulation(
                        (int)$arguments[0] * $multiplier,
                        (int)$arguments[1] * $multiplier,
                        $arguments[2],
                        (int)$arguments[3]
                    );
                }
            }
        }

        $img = $img
            ->customise([
                'YamlImage' => $img,
                'YamlImageID' => uniqid(),
                'Breakpoints' => $data['breakpoints'],
                'LazyLink' => $this->createLazyLink($data['configName']),
            ]);

        return $render
            ? $img->renderWith(__NAMESPACE__ . '\\YamlImage' . (Toolbox::is_backend() ? 'Backend' : ''))
            : $img;
    }

    public function getDimensions(string $configName): array
    {
        $config = Config::config()->get($configName);
        $manipulations = self::get_manipulations($config);

        if (!count($manipulations)) {
            return [];
        }

        $manipulation = array_key_last($manipulations);
        $arguments = explode(',', str_replace(' ', '', $manipulations[$manipulation]));

        if (count($arguments) === 1) {
            return ($manipulation === 'ScaleHeight')
                ? ['Height' => $arguments[0]]
                : ['Width' => $arguments[0]];
        } else {
            return [
                'Width' => $arguments[0],
                'Height' => $arguments[1],
            ];
        }
    }
}
