# Silverstripe Yaml Images

image manipulation and configuration through yaml

## Installation

```
composer require bhofstaetter/yaml-images
```

## Configuration

```yaml
---
Name: app-imagesizes
---
bhofstaetter\YamlImages\Config:
  DummyConfig:
    order: 'ScaleHeight,Pad,Fill' # enables execution and defines order
    Fill: '800,800'
    Pad: '800,450,ff00ff,20'
    ScaleHeight: '300'
    breakpoints:
      '(min-width: 376px) and (max-width: 768px)': DummyConfig_Tablet
      '(max-width: 375px)': DummyConfig_Mobile
```

```yaml
---
Name: app-imagesizes-auto
---
bhofstaetter\YamlImages\AutoConfig:
  grid_settings:
    columns_count: 12
    column_width: 75
    gutter_width: 30
    max_width_on_breakpoint: # todo naming, which, values
      desktop: 1080
      laptop: 920
      tablet: 710
      tablet_small: 450
      mobile: 320

  DummyConfig:
    forBreakpoint: desktop
    order: 'Pad,ScaleHeight,Fill' # enables execution and defines order
    Pad:
      columns: 6
      ratio: '16:9'
      arguments: 'ff00ff,20'
    ScaleHeight:
      arguments: '300'
    Fill:
      columns: 6
      ratio: '1:1'
    breakpoints:
      '(min-width: 376px) and (max-width: 768px)': DummyConfig_Tablet
      '(max-width: 375px)': DummyConfig_Mobile
  DummyConfig_Tablet:
    forBreakpoint: tablet
    Pad:
      columns: 12
      ratio: '16:9'
      arguments: '000000,20'
  DummyConfig_Mobile:
    forBreakpoint: mobile
    ScaleWidth:
      columns: 12
      ratio: '16:4'

```

## Todo

- Shared Draft Link geht nicht. Liegt es an dem Modul?
- Readme
- Breakpoints
